import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test08';
  Form: FormGroup
  userData: any = null
  constructor(private http: HttpClient, private fb: FormBuilder) {
    this.Form = fb.group({
      name: ['']
    })
  }
  async getUser() {
    this.userData = await this.http.get(`https://api.agify.io/?name=${this.Form.value.name}`).toPromise()
    console.log(this.userData, 'userData ');

  }
}
